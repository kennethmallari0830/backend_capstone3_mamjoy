const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: [true, 'First name is required']
  },
  lastName: {
        type: String,
        required: [true, 'Last name is required.']
    },
    email: {
        type: String,
        required: [true, 'Email is required.'],
    },
    password: {
        type: String
        
    },
    balance: {
      type: String,
      required: [true, 'Balance is required.']
      
    },
    loginType:{

        type: String,
        required: [true, 'Log in type is required is required.']
    },
    records: [
      { 
        type: {
                type: String,
                // enum: ["income", "expense"],
                required: [true, "Type is required"]
            },
        category: {
          type: String,
          required: [true, "Category is required"]
        },
        amount: {
          type: String,
          required: [true, "Amount is required"]
        },
        description: {
          type: String,
          required: [true, "Description is required"]
        },
        balance: {
          type: String,
          required: [true, "Balance is required"]
        },
        recordedOn: {
          type: Date,
          required: [true,"Date is required"]

          //default: new Date()
        }
      }
    ],
    categories: [
      {
        name: {
          type:String,
          required: [true, "Category Name is required"]
        },
        type: {
          type: String,
          required: [true, "Category Type is required"]
        }
      }
    ]
})

module.exports = mongoose.model('User', userSchema);



// const mongoose = require('mongoose')

// const userSchema = new mongoose.Schema({

//     firstName: {
//        type: String,
//        required: [true, 'First name is required.']
//    },
//    lastName: {
//        type: String,
//        required: [true, 'Last name is required.']
//    },
//    email: {
//        type: String,
//        required: [true, 'Email is required.']
//    },
//    password: {
//        type: String,
//        required: [true, 'Password is required']
//    },
 
//    mobileNo: {
//        type: String,
//        required: [true, 'Mobile number is required.']
//    },
//     balance: {
//     	type: Number,
    	
//     }


// })

// module.exports = mongoose.model('User', userSchema)