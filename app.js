const express = require ('express');
const mongoose = require('mongoose');
const cors = require('cors');



require ('dotenv').config();
const app = express();
const port = process.env.PORT || 8000;

//Uncomment after

const corsOptions = {

			//Vercel Link
	origin: 'https://capstone-3-client-mallari-jet.vercel.app',
	optionsSuccessStatus: 200
}

	//corsOptions
app.use(cors(corsOptions));
app.options('*',cors())

//connect to Database
mongoose.connect(process.env.DB,{useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false});

//Check connection to Database
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log("Connected to database")
});

//middlewares
app.use(express.json());

// routes

// user route
const userRoutes = require('./routes/user');
app.use('/api/users', userRoutes)




app.listen(port,()=>{
	console.log(`Server is listening on port: ${port}`);
}); 