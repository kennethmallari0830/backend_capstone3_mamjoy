const express = require ('express');
const router = express.Router();
const UserController = require ('./../controllers/user')
const auth = require('./../auth')


//Email Exists
router.post('/email-exists',(req,res) => {
	UserController.emailExists(req.body).then(result => res.send(result))
})


//Register User
router.post('/', (req,res) =>{
	UserController.register(req.body). then(result => res.send(result))
})


//Login
router.post('/login',(req,res)=>{
	// userController.login(req.body).then(result=> res.send(result))
	UserController.login(req,res)
})


// get User details
router.get('/details',auth.verify,(req,res)=>{

	const decodedToken = auth.decode(req.headers.authorization)
	// res.send(decodedToken)
	// console.log(decodedToken);
	UserController.get({userId : decodedToken.id}).then(user => res.send(user))
	
})

//add Category
router.post('/category', auth.verify,(req,res)=>{
	
	UserController.addCategory(req.body).then(result => res.send(result))
})

//add Transaction
router.post('/transaction', auth.verify,(req,res)=>{
	UserController.addTransaction(req.body).then(result => res.send(result))
})


//get all transactions
router.get('/allTransactions',  (req,res)=>{
const decodedToken = auth.decode(req.headers.authorization)
UserController.getAllTransactions({userId : decodedToken.id}).then(user => res.send(user))


} )

//get all Categories
router.get('/allCategories', auth.verify, (req,res)=>{
const decodedToken = auth.decode(req.headers.authorization)
UserController.getAllCategories({userId : decodedToken.id}).then(user => res.send(user))


})

//get balance
router.get('/balance', (req,res)=>{
const decodedToken = auth.decode(req.headers.authorization)
UserController.getAllBalance({userId : decodedToken.id}).then(user => res.send(user))
})


//Update Balance
router.put('/updateBalance', auth.verify,(req,res)=>{

	UserController.updateBalance(req.body).then(result => res.send(result))
})

// find transaction by description
router.post('/post', auth.verify, (req,res)=>{
	UserController.searchTransaction(req.body).then(result => res.send(result))
})

//Google Login
router.post('/verify-google-id-token', async (req,res)=> {
	console.log(req)
	res.send(await UserController.verifyGoogleTokenId(req.body.tokenId,req.body.accessToken))
})


// router.post('/post', auth.verify, (req,res)=>{
// 	const decodedToken = auth.decode(req.headers.authorization)
// 	UserController.searchTransaction({userId: decodedToken.id}, req.body).then(result => res.send(result))
// })







module.exports = router;
