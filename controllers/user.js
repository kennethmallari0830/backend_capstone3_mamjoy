const User = require('../models/user');
const bcrypt = require('bcrypt');
const {createAccessToken} = require('./../auth');
const {OAuth2Client} = require('google-auth-library')
const nodemailer = require('nodemailer')
const clientId="168595206810-02estdan10lnj23o9esplutbvlvtg1t9.apps.googleusercontent.com"


//Email Exist
module.exports.emailExists =(params) => {
		return User.find({email: params.email }).then(result =>{
			return result.length > 0 ? true : false 
		})
}

//Register
module.exports.register = (params) => {
	let newUser = new User({
		firstName: params.firstName,
		lastName:  params.lastName,
		email: 	params.email,
		// 10 - salt/string of characters added to the password before hashing
		password: bcrypt.hashSync(params.password, 10),
		balance: 0,
		loginType: "email"
	})

	return newUser.save().then((user,err)=>{
		return(err) ? false : true
	})
}



//Login
module.exports.login = (req,res) =>{

	User.findOne({email : req.body.email})
	.then( user => {
		if (!user){
			res.send(false)
		}else{
			let comparePasswordResult= bcrypt.compareSync(req.body.password,user.password);
			if(!comparePasswordResult){
				res.send(false)
			}else{
				
				res.send({accessToken: createAccessToken(user)})
				// res.send(true) 
			}
		}
	}).catch(err =>{
		res.status(500).send("Server Error")
	})

}


//Get user Details
module.exports.get =(params) => {
	return User.findById(params.userId).select({password: 0}).then(user => {
		console.log(user);
		
		 return user


	})
}


//Add category
module.exports.addCategory = (params) =>{
	console.log('params',params)
	return User.findById(params.id).then(category =>{
		category.categories.push({name: params.name, type: params.type})
		return category.save().then(result =>{
			console.log('result',result)
			return result ? true : false
		})
	})

}

//Add Transaction
module.exports.addTransaction = (params) =>{

	const  {id, type,category, amount, description, balance, recordedOn} = params
	return User.findById(id).then(transaction=>{

		transaction.records.push({type:type,category: category, amount: amount, description: description, balance: balance, recordedOn: recordedOn})
				return transaction.save().then(result =>{
					return result ? true : false
				})

	})
}

//Get ALL transactions
module.exports.getAllTransactions = (params) =>{

	//main return
	return User.findById(params.userId).then(transaction =>{
		return transaction.records
	})
}

//Get all Transactions
// To get the categories, manipulate the array in the FE
module.exports.getAllCategories = (params) =>{

	//main return
	return User.findById(params.userId).then(category =>{
		return category.categories
	})
}

//Get Balance
module.exports.getAllBalance = (params) =>{

	//main return
	return User.findById(params.userId).then(result =>{
		
				//result nung find
		return result.balance
	})
}


//Update Balance
module.exports.updateBalance = (params) =>{
	console.log(params)

	 return User.findByIdAndUpdate(params.id, {balance: params.balance})
	.then((doc, err )=> {
	 	return err ? false : true
    })
}

//Find transaction by description getAllTransations => .find()
module.exports.getAllTransactions = (params) =>{

	//main return
	return User.findById(params.userId).then(transaction =>{
		return transaction.records
	})
}


//search
module.exports.searchTransaction = (params) =>{


	console.log(params)

	//main return
	 return User.findById(params.userId).then(user =>{
	 	return user.records.find(transaction=>{
	 		if(transaction.description === params.description){
	 			return transaction
			}else{
				return false
			}
		})
	})
}

//Google login
module.exports.verifyGoogleTokenId = async (tokenId,googleAccessToken) => {
	//console.log(tokenId)
	console.log(googleAccessToken)

	const client = new OAuth2Client(clientId)
	const data = await client.verifyIdToken({idToken: tokenId, audience: clientId})


	if(data.payload.email_verified === true){

		const user = await User.findOne({email: data.payload.email}).exec()

		if(user !== null){

			console.log("A user with same email has been registered.")
			console.log(user)

			if(user.loginType ==="google"){

				return {accessToken: createAccessToken(user)}

			}else{

				return {error: 'login-type-error'}

			}
		}else{
	
			 console.log(data.payload)
			 const newUser = new User ({

			 	firstName: data.payload.given_name,
			 	lastName: data.payload.family_name,
			 	email: data.payload.email,
			 	balance: 0,
			 	loginType: "google"
			 })

			 return newUser.save().then((user,err)=>{

			 	const mailOptions = {
			 		from: 'kennethmallari04@gmail.com',
			 		to: user.email, //newly registered user
			 		subject: "Welcome to Sage.ly!",
			 		text: `Welcome to Sage.ly! The date is ${new Date().toLocaleString()}`,
			 		html: `Welcome to Sage.ly! The date is ${new Date().toLocaleString()}`
			 	}

			 	const transporter = nodemailer.createTransport({
			 		host: 'smtp.gmail.com',
			 		port: 465,
			 		secure: true,
			 		auth: {
			 			type: 'OAuth2',
			 			user: process.env.MAILING_SERVICE_EMAIL,
			 			clientId: process.env.MAILING_SERVICE_CLIENT_ID,
			 			clientSecret: process.env.MAILING_SERVICE_CLIENT_SECRET,
			 			refreshToken: process.env.MAILING_SERVICE_REFRESH_TOKEN,
			 			accessToken: googleAccessToken
			 		},
			 		tls: {
			 			rejectUnauthorized: false
			 		}
			 	})

			 	function sendMail(transporter){
			 		transporter.sendMail(mailOptions, function(err, result){
			 			if (err){
			 				console.log(err)
			 				transport.close()
			 			} else if (result) {
			 				console.log(result)
			 				transport.close()
			 			}
			 		})
			 	}

			 	sendMail(transporter)

			 	return {accessToken: createAccessToken(user)}
			 })
		}
	}else{

		return{error: "google-auth-error"}
	}
}






